
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include "mex.h"


void sin_noise_mex_16( short *data, int index, int chan, double Fs, double F, int n )
{
	int range_max = 16;
	int range_min = -16;
	double div = Fs/F;
	double pi = 3.1415926535;

     // Generate sine + noise
     int i;
	 double ph = (double)rand()/32768;
	 double A = 32000.0/index; 
     for ( int i = 0; i < n; i++ )
     {
        int u = (double)rand() / (RAND_MAX + 1) * (range_max - range_min) + range_min;
		double t = (double)i/div;
		int x = A*sin(2*pi*t + ph);
		data[chan*i+(index-1)] = u+x;
		if((i==1022)||(i==1023)) {
			u = u;
		}
     }
}


//---------------------------------------------------------------------------
// prhs[0] - type ('int8', 'int16', 'int32')
//
// plhs[0] - z matrix
// plhs[1] - Fs 
// plhs[2] - chan 
// plhs[3] - size 
//
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//To check for 1 input arguments, use this code.
	if(nrhs != 1) {
		mexErrMsgIdAndTxt("MyToolbox:sig_mex:nrhs",
                      "1 inputs required.");
	}

	// Use this code to check for 4 output argument, the product outMatrix.
	if(nlhs != 4) {
		mexErrMsgIdAndTxt("MyToolbox:sig_mex:nlhs",
                      "4 output required.");
	}

	//This code validates that in1, represented by prhs[0], is type double.
	if( !mxIsChar(prhs[0]) ) {
			mexErrMsgIdAndTxt("MyToolbox:sig_mex:notChar",
				"Input 1 must be type char.");
	}     

	char *in1 = mxArrayToString(prhs[0]);

	plhs[1] = mxCreateDoubleMatrix(1,1,mxREAL);	// Fs 
	plhs[2] = mxCreateNumericMatrix(1, 1, mxINT16_CLASS, mxREAL); // chan
	//plhs[3] = mxCreateNumericMatrix(1, 1, mxINT16_CLASS, mxREAL); // size
	plhs[3] = mxCreateDoubleMatrix(1,1,mxREAL);	// size 

	double *Fs = (double*)mxGetPr(plhs[1]);	
	short int *chan = (short int*)mxGetPr(plhs[2]);	
	double *size = (double*)mxGetPr(plhs[3]);	

	*size = 1024;
	*chan = 4;
	*Fs = 100000000.0;

	int _size = *size;
	int _chan = *chan;
	double _Fs = *Fs;

	if( strcmp(in1,(char*)"int16") == 0) {
		plhs[0] = mxCreateNumericMatrix(_chan,_size, mxINT16_CLASS, mxREAL);
		short int *outMatrix = (short int*)mxGetPr(plhs[0]);		// output matrix 
		for(int i=0;i<_chan;i++) {
			short int *outArray = (outMatrix);//+i*_size);	// current matrix row
			double F = 10000000.0*(i+1);
			sin_noise_mex_16( outArray, i+1, _chan, _Fs, F, _size );
		
		}
	} 

}

