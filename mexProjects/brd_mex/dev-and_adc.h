//********************************************************
//********** dev_and_adc.h **********
//
// High Level DEV_& ADC_ functions 
//
// (C) InSys, 2007-2018
//
//********************************************************

#include	"brd.h"
#include	"extn.h"
#include	"ctrladc.h"
#include	"ctrlsdram.h"
#include	"ctrlstrm.h"

#include	"gipcy.h"

#define		MAX_NAME	32		// �������, ��� ��� ����������/����/������ ����� ���� �� ����� 32 �������� 
#define		MAX_SRV		16		// �������, ��� ����� �� ����� ������ ����� ���� �� ������ MAX_SRV

#pragma pack(push,1)

// ���������� �� ����������
typedef struct
{
	U32			size;			// sizeof(DEV_INFO)
	U16			devID;			// Device ID
	U16			rev;			// Revision
	BRDCHAR		devName[MAX_NAME];	// Device Name
	U32			pid;			// Board Phisical ID
	S32			bus;			// Bus Number
	S32			dev;			// Dev Number
	S32			slot;			// Slot Number
	U08			pwrOn;			// FMC power on/off
	U32			pwrValue;		// FMC power value
	U16			pldVer;			// ADM PLD version
	U16			pldMod;			// ADM PLD modification
	U16			pldBuild;		// ADM PLD build
	BRDCHAR		pldName[MAX_NAME];	// ADM PLD Name
	U16			subType;		// Subunit Type Code
	U16			subVer;			// Subunit version
	U32			subPID;			// Subunit serial number
	BRDCHAR		subName[MAX_NAME];	// Submodule Name
	BRDCHAR		srvName[MAX_NAME][MAX_SRV];	// massive of Service Names
} DEV_INFO, *PDEV_INFO;

// ���������� � ������� ���������� ���
typedef struct
{
	U32		size;			// sizeof(ADC_PARAM)
	U32		chmask;			// mask of ON channels
	U32		clkSrc;			// clock source
	REAL64	clkValue;		// clock value
	REAL64	rate;			// sampling rate
} ADC_PARAM, *PADC_PARAM;

#pragma pack(pop)

// ������� ����������
BRD_Handle DEV_open(BRDCHAR * inifile, int idev, int* numdev);
// �������� ���������� �� �������� ����������
int DEV_info(BRD_Handle hDEV, int idev, DEV_INFO* pdevcfg);
// ������� ����������
int DEV_close(BRD_Handle hDEV);
// ������� ��� � �������� ���������� � ��� 
BRD_Handle ADC_open(BRD_Handle hDEV, BRDCHAR* adcsrv, BRD_AdcCfg* adcfg);
// ���������� ������� ��������� ���
int ADC_set(BRD_Handle hADC, int idev, BRDCHAR* adcsrv, BRDCHAR* inifile, ADC_PARAM* adcpar, int memflag);
// ����������� ��� � ������ ����� (������������� ���������� ���, ��������� � ������������� ���������� JESD204B)
int ADC_prepare(BRD_Handle hADC);
// ���������� ������ ��� ��������� ������ � ��� ����� �����
S32 ADC_allocbuf(BRD_Handle hADC, U64* pbytesBufSize);
// ��������� ���� ������ �� ��� � ��
S32 ADC_read(BRD_Handle hADC, int memflag);
// ������������ ������ ������
S32 ADC_freebuf(BRD_Handle hADC);
// ������� ���
int ADC_close(BRD_Handle hADC);
