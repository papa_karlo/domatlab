
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include "mex.h"

#include	"brd.h"
#include	"extn.h"
#include	"extn_6678.h"
#include	"ctrlstrm.h"
#include	"ctrlreg.h"
#include	"ctrladc.h"
#include	"ctrlcmpsc.h"
#include	<utypes.h>
#include	<gipcy.h>

#include	"dev-and_adc.h"

extern	U32 GetOptions(BRDCHAR *iniFile);
S32	ctrl_decode(BRD_Handle handle, U32 nodeId, char *ctrlName, mxArray *plhs[], const mxArray *prhs[]);

extern	BRDctrl_StreamCBufAlloc g_buf_dscr;


//--------------------------------------------------------------------------
// Entry point BRD library
// --- INPUT ---
// prhs[0] - function name ('init', 'open', 'load' ...)
// prhs[1] - args1
// prhs[2] - args2
// ...
// --- OUTPUT ---
// plhs[0] - return 0
// plhs[1] - return 1
// plhs[2] - return 2
// ...
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//This code validates that in1, represented by prhs[0], is type char.
	if( !mxIsChar(prhs[0]) ) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:notChar",
				"Input 1 must be type char.");
	}     
	char *cmdStr = mxArrayToString(prhs[0]);

	if( strcmp(cmdStr,(char*)"init") == 0) {
		// input arguments
		char *ini_name = mxArrayToString(prhs[1]);
		S32 size = strlen(ini_name);
		BRDCHAR *_iniName = (BRDCHAR*)mxGetPr(prhs[1]);
		BRDCHAR iniName[256];
		ZeroMemory(iniName, sizeof(iniName));
		BRDC_strncpy((BRDCHAR*)iniName, _iniName, size);
		
		
		//BRDCHAR *iniName = (BRDCHAR*)mxGetPr(prhs[1]);
		
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		S32 *retCode = (S32*)mxGetPr(plhs[0]);		// output matrix 
		// call BRD function
		BRD_init(iniName, retCode);
		// check error
		if(*retCode<=0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:init",
				"Can't init Boards.");
		}
	}	else if( strcmp(cmdStr,(char*)"reinit") == 0) {
		// input arguments
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		S32 *retCode = (S32*)mxGetPr(plhs[0]);		// output matrix 
		// call BRD function
		BRD_reinit(retCode);
		// check error
		if(*retCode<=0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:reinit",
				"Can't reinit Boards.");
		}
	} else if( strcmp(cmdStr,(char*)"open") == 0) {
		// input arguments
		U32 *Lid = (U32*)mxGetPr(prhs[1]);
		U32 *Mode = (U32*)mxGetPr(prhs[2]);
		U32 lid = *Lid;
		U32 mode = *Mode;
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call BRD function
		*retCode = BRD_open(lid, mode, NULL);
		// check error
		if(*retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:open",
				"Can't Open Boards LID=%d.",lid);
		}
	} else if( strcmp(cmdStr,(char*)"load") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		BRDCHAR *appName = (BRDCHAR*)mxGetPr(prhs[3]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// chaek argc & argv
		if( nrhs>4 ) {
			U32 *argc = (U32*)mxGetPr(prhs[4]);
			BRDCHAR *argv[16];
			argv[0] = appName;
			for(int i=0;i<(int)(*argc);i++) {
				argv[i+1] = (BRDCHAR*)mxGetPr(prhs[5+i]);
			}
			// call function
			*retCode = BRD_load(*bid, *node, appName, *argc+1, argv);
		} else {
			// call function
			*retCode = BRD_load(*bid, *node, appName, 0, NULL);
		}
		// check error
		if(*retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:load",
				"Can't Load App %s on Boards ID=%d.",appName,bid);
		}

	} else if( strcmp(cmdStr,(char*)"start") == 0) {
		// input arguments
		U32 *Bid = (U32*)mxGetPr(prhs[1]);
		U32 *Node = (U32*)mxGetPr(prhs[2]);
		U32 bid = *Bid;
		U32 node = *Node;
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_start(bid, node);
	} else if( strcmp(cmdStr,(char*)"stop") == 0) {
		// input arguments
		U32 *Bid = (U32*)mxGetPr(prhs[1]);
		U32 *Node = (U32*)mxGetPr(prhs[2]);
		U32 bid = *Bid;
		U32 node = *Node;
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_stop(bid, node);
	} else if( strcmp(cmdStr,(char*)"displayMode") == 0) {
		// input arguments
		U32 *pMode = (U32*)mxGetPr(prhs[1]);
		U32 mode = *pMode;
		// call function
		BRD_displayMode(mode);
	} else if( strcmp(cmdStr,(char*)"reset") == 0) {
		// input arguments
		U32 *Bid = (U32*)mxGetPr(prhs[1]);
		U32 *Node = (U32*)mxGetPr(prhs[2]);
		U32 bid = *Bid;
		U32 node = *Node;
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_reset(bid, node);
	} else if( strcmp(cmdStr,(char*)"close") == 0) {
		// input arguments
		U32 *Bid = (U32*)mxGetPr(prhs[1]);
		U32 bid = *Bid;
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_close(bid);
	} else if( strcmp(cmdStr,(char*)"cleanup") == 0) {
		// input arguments
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_cleanup();
	} else if( strcmp(cmdStr,(char*)"getInfo") == 0) {
		// input arguments
		char *infoName = mxArrayToString(prhs[1]);
		U32 *lid = (U32*)mxGetPr(prhs[2]);
		// call function
		BRD_Info info;
		info.size = sizeof(BRD_Info);
		S32 retCode = BRD_getInfo(*lid, &info);
		if(retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:getInfo",
				"Can't Read Info of Board LID=%d.",lid);
		}
		// output arguments
		if( strcmp(infoName,(char*)"name") == 0) {
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0],BRDC_strlen(info.name));
			BRDC_strcpy(name,info.name);
		} else if( strcmp(infoName,(char*)"base") == 0) {
			plhs[0] = mxCreateNumericMatrix(1, 6, mxUINT32_CLASS, mxREAL);
			U32 *pBase = (U32*)mxGetPr(plhs[0]);		
			for(int i=0;i<6;i++) pBase[i] = (U32)info.base[i];
		} else {
			plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
			U32 *param = (U32*)mxGetPr(plhs[0]);		
			if( strcmp(infoName,(char*)"pid") == 0)			*param = (U32)info.pid&0x0FFFFFFF;
			else if( strcmp(infoName,(char*)"type") == 0)   *param = (U32)info.boardType;
			else if( strcmp(infoName,(char*)"order") == 0)  *param = (U32)info.pid>>28;
			else if( strcmp(infoName,(char*)"bus") == 0)	*param = (U32)info.bus;
			else if( strcmp(infoName,(char*)"dev") == 0)	*param = (U32)info.dev;
			else {
				BRDCHAR *bInfoName = (BRDCHAR*)mxGetPr(prhs[1]);
				mexPrintf("BRD_getInfo: Invalid info field name %s.", bInfoName);
			}
		}
	} else if( strcmp(cmdStr,(char*)"peek") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *addr = (U32*)mxGetPr(prhs[3]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_peek(*bid, *node, *addr);
	} else if( strcmp(cmdStr,(char*)"poke") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *addr = (U32*)mxGetPr(prhs[3]);
		U32 *val = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_poke(*bid, *node, *addr, *val);
	} else if( strcmp(cmdStr,(char*)"writeRAM") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *brd_addr = (U32*)mxGetPr(prhs[3]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[4]);
		U32 *items = (U32*)mxGetPr(prhs[5]);
		U32 *itemsize = (U32*)mxGetPr(prhs[6]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_writeRAM(*bid, *node, *brd_addr, pc_addr, *items, *itemsize);
	} else if( strcmp(cmdStr,(char*)"readRAM") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *brd_addr = (U32*)mxGetPr(prhs[3]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[4]);
		U32 *items = (U32*)mxGetPr(prhs[5]);
		U32 *itemsize = (U32*)mxGetPr(prhs[6]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_readRAM(*bid, *node, *brd_addr, pc_addr, *items, *itemsize);
	} else if( strcmp(cmdStr,(char*)"writeDPRAM") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *offset = (U32*)mxGetPr(prhs[3]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[4]);
		U32 *size = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_writeDPRAM(*bid, *node, *offset, pc_addr, *size);
	} else if( strcmp(cmdStr,(char*)"readDPRAM") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *offset = (U32*)mxGetPr(prhs[3]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[4]);
		U32 *size = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_readDPRAM(*bid, *node, *offset, pc_addr, *size);
	} else if( strcmp(cmdStr,(char*)"write") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_write(*bid, *node, pc_addr, *size, *timeout);
	} else if( strcmp(cmdStr,(char*)"read") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_read(*bid, *node, pc_addr, *size, *timeout);
	} else if( strcmp(cmdStr,(char*)"getMsg") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_getMsg(*bid, *node, pc_addr, size, *timeout);
	} else if( strcmp(cmdStr,(char*)"putMsg") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_putMsg(*bid, *node, pc_addr, size, *timeout);
	} else if( strcmp(cmdStr,(char*)"readFIFO") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		U32 brd_addr = 0;
		*retCode = BRD_readFIFO(*bid, *node, brd_addr, pc_addr, *size, *timeout);
	} else if( strcmp(cmdStr,(char*)"writeFIFO") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *pc_addr = (U32*)mxGetPr(prhs[3]);
		U32 *size = (U32*)mxGetPr(prhs[4]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		U32 brd_addr = 0;
		*retCode = BRD_writeFIFO(*bid, *node, brd_addr, pc_addr, *size, *timeout);
	} else if( strcmp(cmdStr,(char*)"signalGrab") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *sigId = (U32*)mxGetPr(prhs[3]);
		U32 *timeout = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *sigCnt = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		U32 retCode = BRD_signalGrab(*bid, *node, *sigId, sigCnt, *timeout);		
	} else if( strcmp(cmdStr,(char*)"signalWait") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *sigId = (U32*)mxGetPr(prhs[3]);
		U32 *timeout = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *sigCnt = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		U32 retCode = BRD_signalWait(*bid, *node, *sigId, sigCnt, *timeout);		
	} else if( strcmp(cmdStr,(char*)"signalSend") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *sigId = (U32*)mxGetPr(prhs[3]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_signalSend(*bid, *node, *sigId);
	} else if( strcmp(cmdStr,(char*)"signalIack") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *sigId = (U32*)mxGetPr(prhs[3]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_signalIack(*bid, *node, *sigId);
	} else if( strcmp(cmdStr,(char*)"signalFresh") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *sigId = (U32*)mxGetPr(prhs[3]);
		U32 *sigCnt = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_signalFresh(*bid, *node, *sigId, sigCnt);
	} else if( strcmp(cmdStr,(char*)"lidList") == 0) {
		// input arguments
		U32 *item = (U32*)mxGetPr(prhs[1]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, *item, mxUINT32_CLASS, mxREAL);
		U32 *lidList = (U32*)mxGetPr(plhs[0]);		// output items 
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *lidItems = (U32*)mxGetPr(plhs[1]);		// output items 
		// call function
		U32 retCode = BRD_lidList(lidList,*item,lidItems);
	} else if( strcmp(cmdStr,(char*)"extension") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *extCode = (U32*)mxGetPr(prhs[3]);
		U32 *extParam = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_extension(*bid,*node,*extCode,extParam);
	} else if( strcmp(cmdStr,(char*)"ctrl") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		//U32 *ctrlParam = (U32*)mxGetPr(prhs[4]);
		//U32 *ctrlCode = (U32*)mxGetPr(prhs[3]);
		char *ctrlName = mxArrayToString(prhs[3]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		
		// call function control decode 
		*retCode = ctrl_decode(*bid, *node, ctrlName, plhs, prhs);
		
	} else if( strcmp(cmdStr,(char*)"capture") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		U32 *mode = (U32*)mxGetPr(prhs[3]);
		U32 *timeout = (U32*)mxGetPr(prhs[5]);

		//BRDCHAR *servName = (BRDCHAR*)mxGetPr(prhs[4]);
		char *serv_name = mxArrayToString(prhs[4]);
		S32 size = strlen(serv_name);
		BRDCHAR *_servName = (BRDCHAR*)mxGetPr(prhs[4]);
		BRDCHAR servName[256];
		ZeroMemory(servName, sizeof(servName));
		BRDC_strncpy((BRDCHAR*)servName, _servName, size);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *_mode = (U32*)mxGetPr(plhs[1]);		// output matrix 
													// call function
		*retCode = BRD_capture(*bid,*node,mode,servName,*timeout);
		*_mode = *mode;

		if(*retCode<0) {
			char *sname = mxArrayToString(prhs[4]);
			mexErrMsgIdAndTxt("BrdToolbox:brd:capture",
					"Can't capture '%s' (error 0x%08lX).", servName, *retCode);
		}
	} else if( strcmp(cmdStr,(char*)"release") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *node = (U32*)mxGetPr(prhs[2]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_release(*bid,*node);
		if(*retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:release",
					"Can't release (error 0x%08lX).", *retCode);
		}
	} else if( strcmp(cmdStr,(char*)"puLoad") == 0) {
		// input arguments
		U32 *bid = (U32*)mxGetPr(prhs[1]);
		U32 *puid = (U32*)mxGetPr(prhs[2]);
		BRDCHAR *fileName = (BRDCHAR*)mxGetPr(prhs[3]);
		U32 *state = (U32*)mxGetPr(prhs[4]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = BRD_puLoad(*bid,*puid,fileName,state);
		if(*retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:puLoad",
					"Can't puLoad '%s' (error 0x%08lX).", fileName, *retCode);
		}
	} else if( strcmp(cmdStr,(char*)"serviceList") == 0) {
		// input arguments
		char *cmdName = mxArrayToString(prhs[1]);
		U32 *bid = (U32*)mxGetPr(prhs[2]);
		U32 *node = (U32*)mxGetPr(prhs[3]);
	
		if( strcmp(cmdName,(char*)"items") == 0) {
			// output arguments
			BRD_ServList servlist[32];
			plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
			U32 *items = (U32*)mxGetPr(plhs[0]);
			// call function
			BRD_serviceList(*bid,*node,servlist,32, items);
		} else if( strcmp(cmdName,(char*)"service") == 0) {
			// input arguments
			U32 *item = (U32*)mxGetPr(prhs[4]);
			// call function
			BRD_ServList servlist[32];
			U32 items;
			BRD_serviceList(*bid,*node,servlist,32, &items);
			// output arguments
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0],BRDC_strlen(servlist[*item].name));
			BRDC_strcpy(name,servlist[*item].name);
		}
	} else if (strcmp(cmdStr, (char*)"getoptions") == 0) {
		// input arguments
//		BRDCHAR *fileName = (BRDCHAR*)mxGetPr(prhs[1]);
		char *file_name = mxArrayToString(prhs[1]);
		S32 size = strlen(file_name);
		BRDCHAR *_fileName = (BRDCHAR*)mxGetPr(prhs[1]);
		BRDCHAR fileName[256];
		ZeroMemory(fileName, sizeof(fileName));
		BRDC_strncpy((BRDCHAR*)fileName, _fileName, size);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
													// call function
		*retCode = GetOptions(fileName);
		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:getoptions",
				"Can't GetOptions '%s' (error 0x%08lX).", fileName, *retCode);
		}
	}  
	else if (strcmp(cmdStr, (char*)"ADC_allocbuf") == 0) {
		// input arguments
		U32 *handle  = (U32*)mxGetPr(prhs[1]);
		U32 *blkSize = (U32*)mxGetPr(prhs[2]);
		U32 *blkNum  = (U32*)mxGetPr(prhs[3]);
		U32 *dir     = (U32*)mxGetPr(prhs[4]);
		U32 *isCont  = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
												// call function
		void** pBuffer = NULL;
		if (2 == (*isCont)) {
			pBuffer = new PVOID[*blkNum];
			for (ULONG i = 0; i < (*blkNum); i++)
			{
				pBuffer[i] = IPC_virtAlloc((*blkSize));
				if (!pBuffer[i]) {
					*retCode = -1;
				}
			}
		}

		g_buf_dscr.dir = *dir;
		g_buf_dscr.blkNum = *blkNum;
		g_buf_dscr.blkSize = *blkSize;
		g_buf_dscr.ppBlk = new PVOID[g_buf_dscr.blkNum];
		if (2 == (*isCont)) {
			for(ULONG i=0; i < *blkNum; i++) g_buf_dscr.ppBlk[i] = pBuffer[i];
			delete[] pBuffer;
		}

		*retCode = BRD_ctrl(*handle, 0, BRDctrl_STREAM_CBUF_ALLOC, &g_buf_dscr);
		if (BRD_errcmp(*retCode, BRDerr_OK)) {
		}
		else {
			if (2 == (*isCont))
			{
				for (ULONG i = 0; i < (*blkNum); i++) IPC_virtFree(g_buf_dscr.ppBlk[i]);
			}
			delete[] g_buf_dscr.ppBlk;
		}

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:allocbuf",
				"Can't buffer allocate (error 0x%08lX).", *retCode);
		}
	} else if (strcmp(cmdStr, (char*)"freebuf") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		U32 *blkNum = (U32*)mxGetPr(prhs[2]);
		U32 *isCont = (U32*)mxGetPr(prhs[3]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
												// call function
		
		*retCode = BRD_ctrl(*handle, 0, BRDctrl_STREAM_CBUF_FREE, NULL);

		if (2 == (*isCont))
		{
			for (ULONG i = 0; i < (*blkNum); i++)
			{
				IPC_virtFree(g_buf_dscr.ppBlk[i]);
			}
		}
		delete[] g_buf_dscr.ppBlk;

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:freebuf",
				"Can't buffer free (error 0x%08lX).", *retCode);
		}
	} else if (strcmp(cmdStr, (char*)"DEV_open") == 0) {
		// input arguments
		char *ini_name = mxArrayToString(prhs[1]);
		S32 size = strlen(ini_name);
		BRDCHAR *_iniName = (BRDCHAR*)mxGetPr(prhs[1]);
		BRDCHAR iniName[256];
		ZeroMemory(iniName, sizeof(iniName));
		BRDC_strncpy((BRDCHAR*)iniName, _iniName, size);

		U32 *iDev = (U32*)mxGetPr(prhs[2]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *pnumdev = (INT32*)mxGetPr(plhs[1]);		// output matrix 

													
		// call function
		*retCode = DEV_open(iniName, *iDev, pnumdev);

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:DEV_open",
				"Can't Device Open (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"DEV_close") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
		*retCode = DEV_close(*handle);
		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:DEV_close",
				"Can't Device Close (error 0x%08lX).", *retCode);
		}
	} else if (strcmp(cmdStr, (char*)"DEV_info") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		U32 *idev = (U32*)mxGetPr(prhs[2]);
		char *infoName = mxArrayToString(prhs[3]);

		// call function
		DEV_INFO dev_info;
		dev_info.size = sizeof(DEV_INFO);
		S32 retCode = DEV_info(*handle, *idev, &dev_info);
		if (retCode<0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:DEV_info",
				"Can't Read Info of Device iDEV=%d, Ret=%d.", *idev, retCode);
		}
		// output arguments
		if (strcmp(infoName, (char*)"devName") == 0) {
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0], BRDC_strlen(dev_info.devName));
			BRDC_strcpy(name, dev_info.devName);
		}
		else if (strcmp(infoName, (char*)"pldName") == 0) {
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0], BRDC_strlen(dev_info.pldName));
			BRDC_strcpy(name, dev_info.pldName);
		}
		else if (strcmp(infoName, (char*)"subName") == 0) {
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0], BRDC_strlen(dev_info.subName));
			BRDC_strcpy(name, dev_info.subName);
		}
		else if (strcmp(infoName, (char*)"srvItems") == 0) {
			// output arguments
			plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
			U32 *items = (U32*)mxGetPr(plhs[0]);		// output matrix 
			BRD_ServList srvList[MAX_SRV];
			S32 status = BRD_serviceList(*handle, 0, srvList, MAX_SRV, items);
		}
		else if (strcmp(infoName, (char*)"srvName") == 0) {
			U32 *srv_num = (U32*)mxGetPr(prhs[4]);
			plhs[0] = mxCreateString("                                ");
			BRDCHAR *name = (BRDCHAR*)mxGetPr(plhs[0]);
			mxSetN(plhs[0], BRDC_strlen(dev_info.srvName[*srv_num]));
			BRDC_strcpy(name, dev_info.srvName[*srv_num]);
		} else {
			plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
			U32 *param = (U32*)mxGetPr(plhs[0]);
			if (strcmp(infoName, (char*)"devID") == 0)		*param = (U32)dev_info.devID & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"rev") == 0)	*param = (U32)dev_info.rev & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pid") == 0)	*param = (U32)dev_info.pid & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"bus") == 0)	*param = (U32)dev_info.bus & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"dev") == 0)	*param = (U32)dev_info.dev & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"slot") == 0)	*param = (U32)dev_info.slot & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pwrOn") == 0)	*param = (U32)dev_info.pwrOn & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pwrValue") == 0)	*param = (U32)dev_info.pwrValue & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pldVer") == 0)	*param = (U32)dev_info.pldVer & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pldMod") == 0)	*param = (U32)dev_info.pldMod & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"pldBuild") == 0)	*param = (U32)dev_info.pldBuild & 0x0FFFFFFF;
			else if (strcmp(infoName, (char*)"subType") == 0)  *param = (U32)dev_info.subType;
			else if (strcmp(infoName, (char*)"subVer") == 0)  *param = (U32)dev_info.subVer;
			else if (strcmp(infoName, (char*)"subPID") == 0)  *param = (U32)dev_info.subPID;
			else {
				BRDCHAR *bInfoName = (BRDCHAR*)mxGetPr(prhs[1]);
				mexPrintf("DEV_info: Invalid info field name %s.", bInfoName);
			}
		}
	} else if (strcmp(cmdStr, (char*)"ADC_open") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		
		char *adcsrv = mxArrayToString(prhs[2]);
		S32 size = strlen(adcsrv);
		BRDCHAR *_adcsrv = (BRDCHAR*)mxGetPr(prhs[2]);
		BRDCHAR adcSrv[256];
		ZeroMemory(adcSrv, sizeof(adcSrv));
		BRDC_strncpy((BRDCHAR*)adcSrv, _adcsrv, size);
		
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
							
		// call function
		BRD_AdcCfg adc_cfg;
		*retCode = ADC_open(*handle, adcSrv, &adc_cfg);
		 if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_open",
				 "Can't ADC Open (error 0x%08lX).", *retCode);
		 }
	}
	else if (strcmp(cmdStr, (char*)"ADC_cfg") == 0) {
		// input arguments
		U32 *hADC = (U32*)mxGetPr(prhs[1]);
		char *cfgName = mxArrayToString(prhs[2]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
														// call function
		BRD_AdcCfg adc_cfg;
		S32 status = BRD_ctrl(*hADC, 0, BRDctrl_ADC_GETCFG, &adc_cfg);
		if (status < 0) {
			*retCode = status;
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_cfg",
				"Error by ADC_cfg (error 0x%08lX).", *retCode);
			return;
		} 
		if (strcmp(cfgName, (char*)"Bits") == 0)			*retCode = (U32)adc_cfg.Bits;
		else if (strcmp(cfgName, (char*)"Encoding") == 0)	*retCode = (U32)adc_cfg.Encoding;
		else if (strcmp(cfgName, (char*)"MinRate") == 0)	*retCode = (U32)adc_cfg.MinRate;
		else if (strcmp(cfgName, (char*)"MaxRate") == 0)	*retCode = (U32)adc_cfg.MaxRate;
		else if (strcmp(cfgName, (char*)"InpRange") == 0)	*retCode = (U32)adc_cfg.InpRange;
		else if (strcmp(cfgName, (char*)"FifoSize") == 0)	*retCode = (U32)adc_cfg.FifoSize;
		else if (strcmp(cfgName, (char*)"NumChans") == 0)	*retCode = (U32)adc_cfg.NumChans;
		else if (strcmp(cfgName, (char*)"ChanType") == 0)	*retCode = (U32)adc_cfg.ChanType;

	} else if (strcmp(cmdStr, (char*)"ADC_set") == 0) {
		// input arguments
		U32 *hADC = (U32*)mxGetPr(prhs[1]);
		U32 *iDev = (U32*)mxGetPr(prhs[2]);

		char *adcsrv = mxArrayToString(prhs[3]);
		S32 size = strlen(adcsrv);
		BRDCHAR *_adcsrv = (BRDCHAR*)mxGetPr(prhs[3]);
		BRDCHAR adcSrv[256];
		ZeroMemory(adcSrv, sizeof(adcSrv));
		BRDC_strncpy((BRDCHAR*)adcSrv, _adcsrv, size);

		char *ini_name = mxArrayToString(prhs[4]);
		size = strlen(ini_name);
		BRDCHAR *_iniName = (BRDCHAR*)mxGetPr(prhs[4]);
		BRDCHAR iniName[256];
		ZeroMemory(iniName, sizeof(iniName));
		BRDC_strncpy((BRDCHAR*)iniName, _iniName, size);

		U32 *memflag = (U32*)mxGetPr(prhs[5]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *chmask = (U32*)mxGetPr(plhs[1]);		// output matrix 
		plhs[2] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *clkSrc = (U32*)mxGetPr(plhs[2]);		// output matrix 
		plhs[3] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
		REAL64 *clkValue = (REAL64*)mxGetPr(plhs[3]);		// output matrix 
		plhs[4] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
		REAL64 *rate = (REAL64*)mxGetPr(plhs[4]);		// output matrix 

		// call function
		ADC_PARAM adc_param;
		adc_param.size = sizeof(ADC_PARAM);
		*retCode = ADC_set(*hADC, *iDev, adcSrv, iniName, &adc_param, *memflag);
		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_set",
				"Can't ADC Set (error 0x%08lX).", *retCode);
		}
		*chmask = adc_param.chmask;
		*clkSrc = adc_param.clkSrc;
		*clkValue = adc_param.clkValue;
		*rate = adc_param.rate;
	}
	else if (strcmp(cmdStr, (char*)"ADC_prepare") == 0) {
		// input arguments
		U32 *hADC = (U32*)mxGetPr(prhs[1]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
		// call function
		*retCode = ADC_prepare(*hADC);
		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_prepare",
				"Can't ADC Prepare (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_close") == 0) {
		// input arguments
		U32 *hADC = (U32*)mxGetPr(prhs[1]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		INT32 *retCode = (INT32*)mxGetPr(plhs[0]);		// output matrix 
														// call function
		*retCode = ADC_close(*hADC);
		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_close",
				"Can't ADC Close (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_allocbuf") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		U32 *blkSize = (U32*)mxGetPr(prhs[2]);
		U32 *blkNum = (U32*)mxGetPr(prhs[3]);
		U32 *dir = (U32*)mxGetPr(prhs[4]);
		U32 *isCont = (U32*)mxGetPr(prhs[5]);
		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
													// call function
		void** pBuffer = NULL;
		if (2 == (*isCont)) {
			pBuffer = new PVOID[*blkNum];
			for (ULONG i = 0; i < (*blkNum); i++)
			{
				pBuffer[i] = IPC_virtAlloc((*blkSize));
				if (!pBuffer[i]) {
					*retCode = -1;
				}
			}
		}

		g_buf_dscr.dir = *dir;
		g_buf_dscr.blkNum = *blkNum;
		g_buf_dscr.blkSize = *blkSize;
		g_buf_dscr.ppBlk = new PVOID[g_buf_dscr.blkNum];
		if (2 == (*isCont)) {
			for (ULONG i = 0; i < *blkNum; i++) g_buf_dscr.ppBlk[i] = pBuffer[i];
			delete[] pBuffer;
		}

		*retCode = BRD_ctrl(*handle, 0, BRDctrl_STREAM_CBUF_ALLOC, &g_buf_dscr);
		if (BRD_errcmp(*retCode, BRDerr_OK)) {
		}
		else {
			if (2 == (*isCont))
			{
				for (ULONG i = 0; i < (*blkNum); i++) IPC_virtFree(g_buf_dscr.ppBlk[i]);
			}
			delete[] g_buf_dscr.ppBlk;
		}

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:allocbuf",
				"Can't buffer allocate (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_freebuf") == 0) {
		// input arguments
		U32 *handle = (U32*)mxGetPr(prhs[1]);
		U32 *blkNum = (U32*)mxGetPr(prhs[2]);
		U32 *isCont = (U32*)mxGetPr(prhs[3]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
													// call function

		*retCode = BRD_ctrl(*handle, 0, BRDctrl_STREAM_CBUF_FREE, NULL);

		if (2 == (*isCont))
		{
			for (ULONG i = 0; i < (*blkNum); i++)
			{
				IPC_virtFree(g_buf_dscr.ppBlk[i]);
			}
		}
		delete[] g_buf_dscr.ppBlk;

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_freebuf",
				"Can't free ADC buffer (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_read") == 0) {
		// input arguments
		U32 *hADC = (U32*)mxGetPr(prhs[1]);
		U32 *memflag = (U32*)mxGetPr(prhs[2]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
		
		UINT32 _size = g_buf_dscr.blkSize;
		plhs[1] = mxCreateNumericMatrix(1, _size, mxINT16_CLASS, mxREAL); // only 1 channel
		INT16 *outData = (INT16*)mxGetPr(plhs[1]);

		// call function
		*retCode = ADC_read(*hADC, *memflag);
		memcpy(outData, g_buf_dscr.ppBlk[0], _size);

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_read",
				"Can't ADC_read (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_openFile") == 0) {
		// input arguments

		char *file_name = mxArrayToString(prhs[1]);
		U32 size = strlen(file_name);
		BRDCHAR *_fileName = (BRDCHAR*)mxGetPr(prhs[1]);
		BRDCHAR fileName[256];
		ZeroMemory(fileName, sizeof(fileName));
		BRDC_strncpy((BRDCHAR*)fileName, _fileName, size);

		U32 *fileFlag = (U32*)mxGetPr(prhs[2]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		IPC_handle *retCode = (IPC_handle*)mxGetPr(plhs[0]);		// output matrix 

		// call function
		*retCode = IPC_openFile(fileName, *fileFlag);

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_openFile",
				"Can't ADC open file - %s(error 0x%08lX).", file_name, *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_writeFile") == 0) {
		// input arguments
		IPC_handle *file = (IPC_handle*)mxGetPr(prhs[1]);
		U32 *buf  = (U32*)mxGetPr(prhs[2]);
		U32 *size = (U32*)mxGetPr(prhs[3]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 

		// call function
		*retCode = IPC_writeFile(*file, buf, *size);

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_writeFile",
				"Can't ADC write file (error 0x%08lX).", *retCode);
		}
	}
	else if (strcmp(cmdStr, (char*)"ADC_closeFile") == 0) {
		// input arguments
		IPC_handle *file = (IPC_handle*)mxGetPr(prhs[1]);

		// output arguments
		plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *retCode = (U32*)mxGetPr(plhs[0]);		// output matrix 
													// call function
		*retCode = IPC_closeFile(*file);

		if (*retCode < 0) {
			mexErrMsgIdAndTxt("BrdToolbox:brd:ADC_closeFile",
				"Can't ADC close file (error 0x%08lX).", *retCode);
		}
	}
	else {
			mexErrMsgIdAndTxt("BrdToolbox:brd:xxx",
					"Can't find BRD function '%s'.", cmdStr);
	}
}



S32	ctrl_decode(BRD_Handle handle, U32 nodeId, char *ctrlName, mxArray *plhs[], const mxArray *prhs[])
{
	S32 status;

	if (strcmp(ctrlName, "BRDctrl_ADC_GETCFG") == 0) {
		BRD_AdcCfg adccfg;
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *bits = (U32*)mxGetPr(plhs[1]);
		plhs[2] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *encoding = (U32*)mxGetPr(plhs[2]);
		plhs[3] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *minrate  = (U32*)mxGetPr(plhs[3]);
		plhs[4] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *maxrate  = (U32*)mxGetPr(plhs[4]);
		plhs[5] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *inprange = (U32*)mxGetPr(plhs[5]);
		plhs[6] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *fifosize = (U32*)mxGetPr(plhs[6]);
		plhs[7] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *numchans = (U32*)mxGetPr(plhs[7]);
		plhs[8] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL); 
		U32 *chantype = (U32*)mxGetPr(plhs[8]);

		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_GETCFG, &adccfg);

		*bits	  = adccfg.Bits;
		*encoding = adccfg.Encoding;
		*minrate  = adccfg.MinRate;
		*maxrate  = adccfg.MaxRate;
		*inprange = adccfg.InpRange;
		*fifosize = adccfg.FifoSize;
		*numchans = adccfg.NumChans;
		*chantype = adccfg.ChanType;
	} 
	else if (strcmp(ctrlName, "BRDctrl_ADC_READINIFILE") == 0) {

		BRD_IniFile ini_file;
		BRDCHAR iniFilePath[MAX_PATH];
		IPC_getCurrentDir(iniFilePath, sizeof(iniFilePath) / sizeof(BRDCHAR));
		BRDC_strcat(iniFilePath, _BRDC("//"));

		char *file_name = mxArrayToString(prhs[4]);
		S32 size = strlen(file_name);
		BRDCHAR *_fileName = (BRDCHAR*)mxGetPr(prhs[4]);
		BRDCHAR fileName[MAX_PATH];
		ZeroMemory(fileName, sizeof(ini_file.fileName));
		BRDC_strncpy(fileName, _fileName, size);
		BRDC_strcat(iniFilePath, fileName);
		BRDC_strcpy(ini_file.fileName, iniFilePath);
		

		char *section_name = mxArrayToString(prhs[5]);
		size = strlen(section_name);
		BRDCHAR *_sectionName = (BRDCHAR*)mxGetPr(prhs[5]);
		ZeroMemory(ini_file.sectionName, sizeof(ini_file.sectionName));
		BRDC_strncpy(ini_file.sectionName, _sectionName, size);

		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_READINIFILE, &ini_file);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_GETSYNCMODE") == 0) {

		BRD_SyncMode sync_mode;

		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *clkSrc = (U32*)mxGetPr(plhs[1]);
		plhs[2] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
		REAL64 *clkValue = (REAL64*)mxGetPr(plhs[2]);
		plhs[3] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
		REAL64 *rate = (REAL64*)mxGetPr(plhs[3]);

		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_GETSYNCMODE, &sync_mode);
		*clkSrc = sync_mode.clkSrc;
		*clkValue = sync_mode.clkValue;
		*rate = sync_mode.rate;
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_GETCHANMASK") == 0) {
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *chan_mask = (U32*)mxGetPr(plhs[1]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_GETCHANMASK, chan_mask);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_PREPARESTART") == 0) {
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_PREPARESTART, NULL);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_GETSRCSTREAM") == 0) {
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *tetrad = (U32*)mxGetPr(plhs[1]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_GETSRCSTREAM, tetrad);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_FIFORESET") == 0) {
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_FIFORESET, NULL);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_FIFOSTATUS") == 0) {
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *adc_status = (U32*)mxGetPr(plhs[1]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_FIFOSTATUS, adc_status);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_ENABLE") == 0) {
		U32 *enable = (U32*)mxGetPr(prhs[4]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_ENABLE, enable);
	}
	else if (strcmp(ctrlName, "BRDctrl_ADC_ISBITSOVERFLOW") == 0) {
		plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
		U32 *adc_status = (U32*)mxGetPr(plhs[1]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_ADC_ISBITSOVERFLOW, adc_status);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_SETSRC") == 0) {
		U32 *tetrad = (U32*)mxGetPr(prhs[4]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_SETSRC, tetrad);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_SETDRQ") == 0) {
		U32 *flag = (U32*)mxGetPr(prhs[4]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_SETDRQ, flag);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_RESETFIFO") == 0) {
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_RESETFIFO, NULL);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_CBUF_START") == 0) {
		U32 *is_cycle = (U32*)mxGetPr(prhs[4]);
		BRDctrl_StreamCBufStart start_pars;
		start_pars.isCycle = *is_cycle;
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_CBUF_START, NULL);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_CBUF_WAITBUF") == 0) {
		U32 *ms_timeout = (U32*)mxGetPr(prhs[4]);
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_CBUF_WAITBUF, ms_timeout);
	}
	else if (strcmp(ctrlName, "BRDctrl_STREAM_CBUF_STOP") == 0) {
		status = BRD_ctrl(handle, nodeId, BRDctrl_STREAM_CBUF_STOP, NULL);
	}




	return status;
}
