/*****************************************************************************
* (c) 2017, Instrumental Systems, Corporation.  All Rights Reserved. *
*****************************************************************************/

/*****************************************************************************
*  PE_SCOPE - Digital Oscilloscope PE Instrument Driver
*  Original Release: March, 2017
*  By: Do, InSys
*      do.insys@gmail.com
*
*  Modification History:
*
*       April, 2017 - Instrument Driver Created.
*
*****************************************************************************/

#include <string.h>
#include <stdio.h>  
#include <stdlib.h>
#include <math.h>
#include <ctype.h>

#include "brd.h"
#include "extn.h"
#include "utypes.h"
#include "gipcy.h"
#include "ctrladc.h"
#include "ctrlstrm.h"
#include "ctrlsdram.h"


BRDCHAR g_AdcSrvName[64]; // � ������� ������
int g_AdcSrvNum; // ����� ������
BRDCHAR g_SrvName[64];	// ��� ������ ������
BRDctrl_StreamCBufAlloc g_buf_dscr;
int g_Cycle = 0;
int g_DaqIntoMemory = 0;
ULONG g_DrqFlag;
ULONG g_samplesOfChannel = 16384;
ULONG g_bBufSize;
ULONG g_MsTimeout;
int g_transRate;
ULONG g_IsWriteFile = 1;
ULONG g_testSeq;
//ULONG g_bWinSize;
U32 mode = BRDcapt_EXCLUSIVE;
//BRDCHAR g_iniFileName[FILENAME_MAX] = _BRDC("//tadc_c6678.ini");
int g_numChan;
ULONG g_bBlkNum;
S32 g_tetrNum = 0;
U32 g_mod = 0;
U32	g_lidADC = 0;

// �������� ��������� �� ini-�����
U32 GetOptions(BRDCHAR *iniFile);


// �������� �������� ���� ������ �� ini-�����:
//    FileName - ��� ini-����� (� �����, ���� �����)
//    SectionName - �������� ������
//    ParamName - �������� ��������� (�����)
//    defValue - �������� ��������� ��-��������� (���� ��������� � ����� ���)
//    strValue - �������� ���������
//    strSize - ������������ ����� ���������
static void GetInifileString(const BRDCHAR *FileName, const BRDCHAR *SectionName, const BRDCHAR *ParamName, BRDCHAR *defValue, BRDCHAR *strValue, int strSize)
{
	IPC_getPrivateProfileString(SectionName, ParamName, defValue, strValue, strSize, FileName);
	BRDCHAR* pChar = BRDC_strchr(strValue, ';');
	if (pChar) *pChar = 0; // ������� ����������� �� ������
	int str_size = (int)BRDC_strlen(strValue);
	for (int i = str_size - 1; i > 1; i--)
		if (strValue[i] != ' ' && strValue[i] != '\t')
		{
			strValue[i + 1] = 0;
			break;
		}
}

// �������� ��������� �� ini-�����
U32 GetOptions(BRDCHAR *iniFileName)
{
	BRDCHAR Buffer[180];
	//BRDCHAR* endptr;
	BRDCHAR iniFilePath[MAX_PATH];

	IPC_getCurrentDir(iniFilePath, sizeof(iniFilePath) / sizeof(BRDCHAR));
	BRDC_strcat(iniFilePath, _BRDC("\\"));
	BRDC_strcat(iniFilePath, iniFileName);
	GetInifileString(iniFilePath, _BRDC("Option"), _BRDC("AdcServiceName"), _BRDC("FM412X500M"), g_SrvName, sizeof(g_SrvName));
	
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("AdcServiceNum"), _BRDC("0"), Buffer, sizeof(Buffer), iniFilePath);
	g_AdcSrvNum = BRDC_atoi(Buffer);
	BRDC_sprintf(g_AdcSrvName, _BRDC("%s%d"), g_SrvName, g_AdcSrvNum);

	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("Cycle"), _BRDC("0"), Buffer, sizeof(Buffer), iniFilePath);
	g_Cycle = BRDC_atoi(Buffer);

	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("DaqIntoMemory"), _BRDC("0"), Buffer, sizeof(Buffer), iniFilePath);
	g_DaqIntoMemory = BRDC_atoi(Buffer);

	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("SamplesPerChannel"), _BRDC("16384"), Buffer, sizeof(Buffer), iniFilePath);
	g_samplesOfChannel = BRDC_atoi(Buffer);
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("DrqFlag"), _BRDC("2"), Buffer, sizeof(Buffer), iniFilePath);
	g_DrqFlag = BRDC_atoi(Buffer);
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("RateRate"), _BRDC("0"), Buffer, sizeof(Buffer), iniFilePath);
	g_transRate = BRDC_atoi(Buffer);
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("TimeoutSec"), _BRDC("5"), Buffer, sizeof(Buffer), iniFilePath); // sec
	g_MsTimeout = BRDC_atoi(Buffer) * 1000;

	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("IsWriteFile"), _BRDC("1"), Buffer, sizeof(Buffer), iniFilePath);
	g_IsWriteFile = BRDC_atoi(Buffer);
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("TestSeq"), _BRDC("0"), Buffer, sizeof(Buffer), iniFilePath);
	g_testSeq = BRDC_atoi(Buffer);
	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("BlockNum"), _BRDC("8"), Buffer, sizeof(Buffer), iniFilePath);
	g_bBlkNum = BRDC_atoi(Buffer);

	IPC_getPrivateProfileString(_BRDC("Option"), _BRDC("LidADC"), _BRDC("1"), Buffer, sizeof(Buffer), iniFilePath);
	g_lidADC = BRDC_atoi(Buffer);

	return 0;
}

