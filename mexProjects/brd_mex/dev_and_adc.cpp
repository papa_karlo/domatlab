//********************************************************
//********** dev_and_adc.cpp **********
//
// High Level DEV_& ADC_ functions 
//
// (C) InSys, 2007-2018
//
//********************************************************

#include	"brd.h"
#include	"extn.h"
#include	"ctrladc.h"
#include	"ctrlsdram.h"
#include	"ctrlstrm.h"

#include	"gipcy.h"

#define		MAX_NAME	32		// �������, ��� ��� ����������/����/������ ����� ���� �� ����� 32 �������� 
#define		MAX_SRV		16		// �������, ��� ����� �� ����� ������ ����� ���� �� ������ MAX_SRV

#pragma pack(push,1)

// ���������� �� ����������
typedef struct
{
	U32			size;			// sizeof(DEV_INFO)
	U16			devID;			// Device ID
	U16			rev;			// Revision
	BRDCHAR		devName[MAX_NAME];	// Device Name
	U32			pid;			// Board Phisical ID
	S32			bus;			// Bus Number
	S32			dev;			// Dev Number
	S32			slot;			// Slot Number
	U08			pwrOn;			// FMC power on/off
	U32			pwrValue;		// FMC power value
	U16			pldVer;			// ADM PLD version
	U16			pldMod;			// ADM PLD modification
	U16			pldBuild;		// ADM PLD build
	BRDCHAR		pldName[MAX_NAME];	// ADM PLD Name
	U16			subType;		// Subunit Type Code
	U16			subVer;			// Subunit version
	U32			subPID;			// Subunit serial number
	BRDCHAR		subName[MAX_NAME];	// Submodule Name
	BRDCHAR		srvName[MAX_NAME][MAX_SRV];	// massive of Service Names
} DEV_INFO, *PDEV_INFO;

// ���������� � ������� ���������� ���
typedef struct
{
	U32		size;			// sizeof(ADC_PARAM)
	U32		chmask;			// mask of ON channels
	U32		clkSrc;			// clock source
	REAL64	clkValue;		// clock value
	REAL64	rate;			// sampling rate
} ADC_PARAM, *PADC_PARAM;

#pragma pack(pop)

extern	BRDctrl_StreamCBufAlloc g_buf_dscr;

//=************************* DEV_& ADC_ functions *************************

#define		MAX_DEV		12		// �������, ��� ������� ����� ���� �� ������ MAX_DEV
#define		MAX_PU		8		// �������, ��� PU-��������� (����, ����) �� ����� ������ ����� ���� �� ������ MAX_PU

// ������� ����������
// inifile - (IN) ���� ������������� c ��������� ������� ����������
// iDev - (IN) ���������� ����� LID � ��������� ����� c ��������� ������� ����������
// pnumdev - (OUT) ���������� ����� ����� ���������
// ���������� ���������� ����������
BRD_Handle DEV_open(BRDCHAR* inifile, int iDev, S32* pnumdev)
{
	S32		status;

	BRD_displayMode(BRDdm_VISIBLE | BRDdm_CONSOLE); // ����� ������ �������������� ��������� : ���������� ��� ������ �� �������

													//status = BRD_initEx(BRDinit_AUTOINIT, inifile, NULL, &DevNum); // ���������������� ���������� - �����������������
	status = BRD_init(inifile, pnumdev); // ���������������� ����������
	if (!BRD_errcmp(status, BRDerr_OK))
		return -1;
	if (iDev >= *pnumdev)
		return -1;

	// �������� ������ LID (������ ������ ������������� ����������)
	BRD_LidList lidList;
	lidList.item = MAX_DEV;
	lidList.pLID = new U32[MAX_DEV];
	status = BRD_lidList(lidList.pLID, lidList.item, &lidList.itemReal);

	BRD_Handle handle = BRD_open(lidList.pLID[iDev], BRDopen_SHARED, NULL); // ������� ���������� � ����������� ������
																			//	if(handle > 0)
																			//	{
																			// �������� ������ �����
																			//U32 ItemReal;
																			//BRD_ServList srvList[MAX_SRV];
																			//status = BRD_serviceList(handle, 0, srvList, MAX_SRV, &ItemReal);
																			//if (ItemReal <= MAX_SRV)
																			//{
																			//	for (U32 j = 0; j < ItemReal; j++)
																			//		BRDC_strcpy(g_srvName[j], srvList[j].name);
																			//}
																			//else
																			//	BRDC_printf(_BRDC("BRD_serviceList: Real Items = %d (> 16 - ERROR!!!)\n"), ItemReal);
																			//	}
	delete lidList.pLID;
	return handle;
}

// ��������������� ������� ��� DEV_info
//=***************************************************************************************
void SubmodName(ULONG id, BRDCHAR * str)
{
	switch (id)
	{
	case 0x1010:    BRDC_strcpy(str, _BRDC("FM814x125M")); break;
	case 0x1012:    BRDC_strcpy(str, _BRDC("FM814x250M")); break;
	case 0x1020:    BRDC_strcpy(str, _BRDC("FM214x250M")); break;
	case 0x1030:    BRDC_strcpy(str, _BRDC("FM412x500M")); break;
	case 0x1040:    BRDC_strcpy(str, _BRDC("FM212x1G")); break;
	case 0x1050:    BRDC_strcpy(str, _BRDC("FM816x250M")); break;
	case 0x1051:    BRDC_strcpy(str, _BRDC("FM416x250M")); break;
	case 0x1052:    BRDC_strcpy(str, _BRDC("FM216x250MDA")); break;
	case 0x1053:    BRDC_strcpy(str, _BRDC("FM816x250M1")); break;
	case 0x10C0:    BRDC_strcpy(str, _BRDC("FM212x4GDA")); break;
	case 0x10C8:    BRDC_strcpy(str, _BRDC("FM214x1GTRF")); break;
	case 0x10D0:    BRDC_strcpy(str, _BRDC("FM112x2G6DA")); break;
	default: BRDC_strcpy(str, _BRDC("UNKNOW")); break;
	}
}

// �������� ���������� �� �������� ����������
// hDEV - (IN) ���������� ��������� ����������
// iDev - (IN) ���������� ����� LID (� ������� �����) c ��������� ������� ����������
// pdevcfg - (OUT) ����������� ����������� �� ���������� ���������
// srv_name - (OUT) ������ ���� �����
//int DEV_info(BRD_Handle hDEV, int iDev, DEV_INFO* pdevcfg, BRDCHAR srv_name[][MAX_SRV])
int DEV_info(BRD_Handle hDEV, int iDev, DEV_INFO* pdevcfg)
{
	S32		status;

	BRD_Info	info;
	info.size = sizeof(info);

	// �������� ������ LID (������ ������ ������������� ����������)
	BRD_LidList lidList;
	lidList.item = MAX_DEV;
	lidList.pLID = new U32[MAX_DEV];
	status = BRD_lidList(lidList.pLID, lidList.item, &lidList.itemReal);

	BRD_getInfo(lidList.pLID[iDev], &info); // �������� ���������� �� ����������
	pdevcfg->devID = info.boardType >> 16;
	pdevcfg->rev = info.boardType & 0xff;
	BRDC_strcpy(pdevcfg->devName, info.name);
	pdevcfg->pid = info.pid;
	pdevcfg->bus = info.bus;
	pdevcfg->dev = info.dev;
	pdevcfg->slot = info.slot;
	pdevcfg->subType = info.subunitType[0];

	BRDCHAR subName[MAX_NAME];
	BRDC_strcpy(pdevcfg->subName, _BRDC("NONE"));
	if (info.subunitType[0] != 0xffff)
	{
		SubmodName(info.subunitType[0], subName);
		BRDC_strcpy(pdevcfg->subName, subName);
	}

	BRDC_strcpy(pdevcfg->pldName, _BRDC("ADM PLD"));
	U32 ItemReal;
	BRD_PuList PuList[MAX_PU];
	status = BRD_puList(hDEV, PuList, MAX_PU, &ItemReal); // �������� ������ ��������������� ���������
	if (ItemReal <= MAX_PU)
	{
		for (U32 j = 0; j < ItemReal; j++)
		{
			U32	PldState;
			status = BRD_puState(hDEV, PuList[j].puId, &PldState); // �������� ��������� ����
			if (PuList[j].puId == 0x100 && PldState)
			{// ���� ��� ���� ADM � ��� ���������
				BRDC_strcpy(pdevcfg->pldName, PuList[j].puDescription);
				pdevcfg->pldVer = 0xFFFF;
				BRDextn_PLDINFO pld_info;
				pld_info.pldId = 0x100;
				status = BRD_extension(hDEV, 0, BRDextn_GET_PLDINFO, &pld_info);
				if (BRD_errcmp(status, BRDerr_OK))
				{
					pdevcfg->pldVer = pld_info.version;
					pdevcfg->pldMod = pld_info.modification;
					pdevcfg->pldBuild = pld_info.build;
				}
			}
			if (PuList[j].puId == 0x03)
			{// ���� ��� ICR ���������
				if (PldState)
				{ // � ��� ��������� �������
					char subICR[14];
					status = BRD_puRead(hDEV, PuList[j].puId, 0, subICR, 14);
					U16 tagICR = *(U16*)(subICR);
					pdevcfg->subPID = *(U32*)(subICR + 7); // �������� ����� ���������
					pdevcfg->subType = *(U16*)(subICR + 11);  // ��� ���������
					pdevcfg->subVer = *(U08*)(subICR + 13);   // ������ ���������
					SubmodName(pdevcfg->subType, subName);
					BRDC_strcpy(pdevcfg->subName, subName);
				}
			}
		}
	}
	delete lidList.pLID;

	// �������� ��������� FMC-������� (���� �� FMC-������, �� ������)
	pdevcfg->pwrOn = 0xFF;
	BRDextn_FMCPOWER power;
	power.slot = 0;
	status = BRD_extension(hDEV, 0, BRDextn_GET_FMCPOWER, &power);
	if (BRD_errcmp(status, BRDerr_OK))
	{
		pdevcfg->pwrOn = power.onOff;
		pdevcfg->pwrValue = power.value;
	}

	// �������� ������ �����
	BRD_ServList srvList[MAX_SRV];
	status = BRD_serviceList(hDEV, 0, srvList, MAX_SRV, &ItemReal);
	if (ItemReal <= MAX_SRV)
	{
		U32 j = 0;
		for (j = 0; j < ItemReal; j++)
			BRDC_strcpy(pdevcfg->srvName[j], srvList[j].name);
		BRDC_strcpy(pdevcfg->srvName[j], _BRDC(""));
	}
	else
		BRDC_printf(_BRDC("BRD_serviceList: Real Items = %d (> 16 - ERROR!!!)\n"), ItemReal);

	return 0;
}

// ������� ����������
int DEV_close(BRD_Handle hDEV)
{
	S32		status;
	status = BRD_close(hDEV); // ������� ���������� 
	status = BRD_cleanup();
	return 0;
}

// ������� ��� � �������� ���������� � ��� 
// hDEV - (IN) ���������� ��������� ����������
// adcsrv - (IN) ��� ������ ���
// adcfg - (OUT) ����������� ����������� �� ��� ���������
// ���������� ���������� ������ ���
BRD_Handle ADC_open(BRD_Handle hDEV, BRDCHAR* adcsrv, BRD_AdcCfg* adcfg)
{
	S32		status;
	BRD_Handle hADC = -1;
	U32 mode = BRDcapt_EXCLUSIVE;
	hADC = BRD_capture(hDEV, 0, &mode, adcsrv, 10000);
	if (hADC > 0)
	{
		if (mode != BRDcapt_EXCLUSIVE)
			return -1;
		status = BRD_ctrl(hADC, 0, BRDctrl_ADC_GETCFG, adcfg);
	}
	return hADC;
}

// ��������� ����������� �� ������������ ������ �� ���������� �
// ������������� �� � �������� FIFO ��� ���
int SDRAM_set(BRD_Handle hADC)
{
	S32		status = 0;

	// ��������� ������� ������������ ������
	BRD_SdramCfgEx SdramConfig;
	SdramConfig.Size = sizeof(BRD_SdramCfgEx);
	ULONG PhysMemSize;
	status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_GETCFGEX, &SdramConfig);
	if (status < 0)
	{
		BRDC_printf(_BRDC("Get SDRAM Config: Error!!!\n"));
		return -1;
	}
	else
	{
		if (SdramConfig.MemType == 11 || //DDR3
			SdramConfig.MemType == 12) //DDR4
			PhysMemSize = (ULONG)((
			(((__int64)SdramConfig.CapacityMbits * 1024 * 1024) >> 3) *
				(__int64)SdramConfig.PrimWidth / SdramConfig.ChipWidth * SdramConfig.ModuleBanks *
				SdramConfig.ModuleCnt) >> 2); // � 32-������ ������
		else
			PhysMemSize = (1 << SdramConfig.RowAddrBits) *
			(1 << SdramConfig.ColAddrBits) *
			SdramConfig.ModuleBanks *
			SdramConfig.ChipBanks *
			SdramConfig.ModuleCnt * 2; // � 32-������ ������
	}
	if (PhysMemSize)
	{ // ������������ ������ ������������ �� ������
		BRDC_printf(_BRDC("SDRAM Config: Memory size = %d MBytes\n"), (PhysMemSize / (1024 * 1024)) * 4);

		// ���������� ��������� SDRAM
		ULONG target = 2; // ����� ������������ ���� ������ � ������
		status = BRD_ctrl(hADC, 0, BRDctrl_ADC_SETTARGET, &target);

		ULONG fifo_mode = 1; // ������ ������������ ��� FIFO
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_SETFIFOMODE, &fifo_mode);
		status = 0;

		BRDC_printf(_BRDC("Memory like FIFO mode!!!\n"));
	}
	else
	{
		// ���������� ������ SDRAM (��� ����� ���� ��������� �������� BRDctrl_SDRAM_GETCFG, ���� �� ���������� ��� ������)
		ULONG mem_size = 0;
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_SETMEMSIZE, &mem_size);
		BRDC_printf(_BRDC("No SDRAM on board!!!\n"));
		status = -2;
	}
	return status;
}

// ���������� ������� ��������� ���
// hADC - (IN) ���������� ������ ���
// iDev - (IN) ���������� ����� ���������� 
// adcsrv - (IN) ��� ������ ���
// inifile - (IN) ���� ������������� c ����������� ������ ���
// adcpar - (OUT) ����������� �������� ����������� ��� ���������
// memflag - (IN) ���� ������������� ������������ ������ � �������� FIFO 
int ADC_set(BRD_Handle hADC, int iDev, BRDCHAR* adcsrv, BRDCHAR* inifile, ADC_PARAM* adcpar, int memflag)
{
	S32		status;

	// ������ ��������� ������ ��� �� ������ ini-�����
	BRDCHAR iniFilePath[MAX_PATH];
	BRDCHAR iniSectionName[MAX_PATH];
	IPC_getCurrentDir(iniFilePath, sizeof(iniFilePath) / sizeof(BRDCHAR));
	BRDC_strcat(iniFilePath, _BRDC("//"));
	BRDC_strcat(iniFilePath, inifile);
	BRDC_sprintf(iniSectionName, _BRDC("device%d_%s"), iDev, adcsrv);

	BRD_IniFile ini_file;
	BRDC_strcpy(ini_file.fileName, iniFilePath);
	BRDC_strcpy(ini_file.sectionName, iniSectionName);
	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_READINIFILE, &ini_file);

	// �������� �������� � �������� �������� �������, � ����� ������� ������������� ����� ��������� ��������
	BRD_SyncMode sync_mode;
	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_GETSYNCMODE, &sync_mode);
	if (!BRD_errcmp(status, BRDerr_OK))
		return -1;
	// �������� ����� ���������� �������
	ULONG chan_mask = 0;
	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_GETCHANMASK, &chan_mask);
	if (!BRD_errcmp(status, BRDerr_OK))
		return -1;

	adcpar->chmask = chan_mask;
	adcpar->clkSrc = sync_mode.clkSrc;
	adcpar->clkValue = sync_mode.clkValue;
	adcpar->rate = sync_mode.rate;

	if (memflag)
	{
		if (SDRAM_set(hADC) < 0)
			return -1;
	}
	return 0;
}

int ADC_prepare(BRD_Handle hADC)
{
	S32		status;

	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_PREPARESTART, NULL);
	if (status < 0)
		if (!(BRD_errcmp(status, BRDerr_CMD_UNSUPPORTED)
			|| BRD_errcmp(status, BRDerr_INSUFFICIENT_SERVICES)))
		{
			return -1;
		}

	return 0;
}

// ������� ���
int ADC_close(BRD_Handle hADC)
{
	S32		status;
	status = BRD_release(hADC, 0);
	return status;
}

#ifdef _WIN32
#define MAX_BLOCK_SIZE 1073741824		// ������������ ������ ����� = 1 ����� 
#else  // LINUX
#define MAX_BLOCK_SIZE 4194304			// ������������ ������ ����� = 4 M����a 
#endif

// ���������� ������ ��� ��������� ������ � ��� ����� �����
//	hADC - ���������� ������ ��� (IN)
//	pSig - ��������� �� ������ ���������� (IN), ������ ������� ������� �������� ���������� �� ���� (OUT)
//	pbytesBufSize - ����� ������ ������ (���� ������ ���������� ������), ������� ������ ���� �������� (IN/OUT - ����� �������� ������ �������)
//	pBlkNum - ����� ������ ���������� ������ (IN/OUT)
//	memType - ��� ������ ��� ������ (IN): 
//		0 - ���������������� ������ ���������� � �������� (������, � DLL �������� ������)
//		1 - ��������� ������ ���������� �������� 0-�� ������
//		2 - ���������������� ������ ���������� � ����������
//S32 ADC_allocbuf(BRD_Handle hADC, PVOID* &pSig, unsigned long long* pbytesBufSize, int* pBlkNum, int memType)
//S32 ADC_allocbuf(BRD_Handle hADC, PVOID* &pSig, U64* pbytesBufSize)
S32 ADC_allocbuf(BRD_Handle hADC, U64* pbytesBufSize)
{
	S32		status;

	unsigned long long bBufSize = *pbytesBufSize;
	ULONG bBlkSize;
	ULONG blkNum = 1;
	// ���������� ����� ������ ���������� ������
	if (bBufSize > MAX_BLOCK_SIZE)
	{
		do {
			blkNum <<= 1;
			bBufSize >>= 1;
		} while (bBufSize > MAX_BLOCK_SIZE);
	}
	bBlkSize = (ULONG)bBufSize;

	void** pBuffer = NULL;
	if (2 == g_buf_dscr.isCont)
	{
		pBuffer = new PVOID[blkNum];
		for (ULONG i = 0; i < blkNum; i++)
		{
			pBuffer[i] = IPC_virtAlloc(bBlkSize);
			if (!pBuffer[i])
				return -1; // error
		}
	}
	g_buf_dscr.dir = BRDstrm_DIR_IN;
	//g_buf_dscr.isCont = memType;
	g_buf_dscr.blkNum = blkNum;
	g_buf_dscr.blkSize = bBlkSize;//*pbytesBufSize;
	g_buf_dscr.ppBlk = new PVOID[g_buf_dscr.blkNum];
	if (g_buf_dscr.isCont == 2)
	{
		for (ULONG i = 0; i < blkNum; i++)
			g_buf_dscr.ppBlk[i] = pBuffer[i];
		delete[] pBuffer;
	}
	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_CBUF_ALLOC, &g_buf_dscr);
	if (BRD_errcmp(status, BRDerr_PARAMETER_CHANGED))
	{ // ����� ���� �������� ������� ���������� ������
		BRDC_printf(_BRDC("Warning!!! BRDctrl_STREAM_CBUF_ALLOC: BRDerr_PARAMETER_CHANGED\n"));
		status = BRDerr_OK;
	}
	else
	{
		if (BRD_errcmp(status, BRDerr_OK))
		{
			//BRDC_printf(_BRDC("BRDctrl_STREAM_CBUF_ALLOC SUCCESS: status = 0x%08X\n"), status);
		}
		else
		{ // ��� ��������� ������ ��������� ������
			if (2 == g_buf_dscr.isCont)
			{
				for (ULONG i = 0; i < blkNum; i++)
					IPC_virtFree(g_buf_dscr.ppBlk[i]);
			}
			delete[] g_buf_dscr.ppBlk;
			return status;
		}
	}
	//pSig = new PVOID[blkNum];
	//for (ULONG i = 0; i < blkNum; i++)
	//{
	//	pSig[i] = g_buf_dscr.ppBlk[i];
	//}
	*pbytesBufSize = (unsigned long long)g_buf_dscr.blkSize * blkNum;
	//*pBlkNum = blkNum;
	return status;
}

// ������������ ������ ������
S32 ADC_freebuf(BRD_Handle hADC)
{
	S32		status;
	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_CBUF_FREE, NULL);
	if (g_buf_dscr.isCont == 2)
	{
		for (ULONG i = 0; i < g_buf_dscr.blkNum; i++)
			IPC_virtFree(g_buf_dscr.ppBlk[i]);
	}
	delete[] g_buf_dscr.ppBlk;
	return status;
}

// ��������� ���� ������ �� ��� � ��
// hADC -  (IN) ���������� ������ ���
// memflag - (IN) ���� ������������� ������������ ������ � �������� FIFO 
S32 ADC_read(BRD_Handle hADC, int memflag)
{
	S32	status = 0;
	ULONG adc_status = 0;
	ULONG sdram_status = 0;
	S32	wait_status = 0;
	ULONG Enable = 1;

	// ���������� �������� ��� ������ ������
	ULONG tetrad;
	if (memflag)
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_GETSRCSTREAM, &tetrad); // ����� ����� �������� � SDRAM
	else
		status = BRD_ctrl(hADC, 0, BRDctrl_ADC_GETSRCSTREAM, &tetrad); // ����� ����� �������� � ���

	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_SETSRC, &tetrad);

	// ������������� ���� ��� ������������ ������� ��� ���� ����� ��������� ��������� (�������) ��� ������ ������
	//	ULONG flag = BRDstrm_DRQ_ALMOST; // FIFO ����� ������
	//	ULONG flag = BRDstrm_DRQ_READY;
	ULONG flag = BRDstrm_DRQ_HALF; // ������������� ���� - FIFO ���������� ���������
	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_SETDRQ, &flag);

	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_FIFORESET, NULL); // ����� FIFO ���
	if (memflag)
	{
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_FIFORESET, NULL); // ����� FIFO SDRAM
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_ENABLE, &Enable); // ���������� ������ � SDRAM
	}
	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_RESETFIFO, NULL);

	BRDctrl_StreamCBufStart start_pars;
	start_pars.isCycle = 0; // ��� ������������ 
							//start_pars.isCycle = 1; // � �������������
	status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_CBUF_START, &start_pars); // ����� ���

	IPC_TIMEVAL start;
	IPC_TIMEVAL stop;
	IPC_getTime(&start);
	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_ENABLE, &Enable); // ���������� ������ ���

	ULONG msTimeout = 1000; // ����� ��������� �������� ������ �� 1 ���. (������ BRDctrl_STREAM_CBUF_WAITBUF)
	int i = 0;	// ���������� ����, ����� ����� ����������� ����������
	for (i = 0; i < 20; i++)
	{
		wait_status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_CBUF_WAITBUF, &msTimeout);
		if (BRD_errcmp(wait_status, BRDerr_OK))
			break;	// ��������� ��������� �������� ������
		if (IPC_kbhit())
		{
			int ch = IPC_getch();
			if (0x1B == ch)
				break;	// ��������� �������� �� Esc
		}
	}
	IPC_getTime(&stop);

	if (BRD_errcmp(wait_status, BRDerr_WAIT_TIMEOUT))
	{	// ���� ����� �� ����-����, �� �����������
		status = BRD_ctrl(hADC, 0, BRDctrl_STREAM_CBUF_STOP, NULL);
		status = BRD_ctrl(hADC, 0, BRDctrl_ADC_FIFOSTATUS, &adc_status);
		BRDC_printf(_BRDC("BRDctrl_STREAM_CBUF_WAITBUF is TIME-OUT(%d sec.)\n    AdcFifoStatus = %08X"),
			msTimeout*(i + 1) / 1000, adc_status);
		if (memflag)
		{
			status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_FIFOSTATUS, &sdram_status);
			BRDC_printf(_BRDC("SdramFifoStatus = %08X\n"), sdram_status);
		}
	}

	Enable = 0;
	status = BRD_ctrl(hADC, 0, BRDctrl_ADC_ENABLE, &Enable); // ������ ������ ���
	if (memflag)
		status = BRD_ctrl(hADC, 0, BRDctrl_SDRAM_ENABLE, &Enable); // ������ ������ � SDRAM

	if (BRD_errcmp(wait_status, BRDerr_OK))
	{
		double msTime = IPC_getDiffTime(&start, &stop);
		U64 bBufSize = g_buf_dscr.blkSize * g_buf_dscr.blkNum;
		printf("DAQ & Transfer by bus rate is %.2f Mbytes/sec\n", ((double)bBufSize / msTime) / 1000.);

		// ��������� ������������ ��������� ����� ���
		status = BRD_ctrl(hADC, 0, BRDctrl_ADC_ISBITSOVERFLOW, &adc_status);
		if (adc_status)
			printf("ADC Bits OVERFLOW %X  ", adc_status);

	}
	return wait_status;
}

//
// End of file
//
