function sig_fft_4
for j = 1:1:20000000
    %[z,Fs,chan,size] = sin_int16(); % function
    [z,Fs,chan,size] = sig_mex('int16'); % function
    subplot(2,1,1); 
    cla
    hold on
    for i=1:1:chan
        plot([1:size],z(i,:));
    end
    hold off
    grid on;
    % ----- calculate spectrum
    for i=1:1:chan
        Y(i,:) = double(z(i,:))/32768;
        w = blackman(size);%������� ����
        Y(i,:)=(w'.*Y(i,:));%������������ ��������
        Y(i,:) = fft(Y(i,:));
        Pyy(i,:) = Y(i,:).*conj(Y(i,:))/(size*size);
    end
    f = Fs*(1:(size/2))/size;
    %f = 1:(size/2);
    subplot(2,1,2); 
    for i=1:1:chan
        b = max(Pyy(i,:));
        P(i,:) = 10*log10(Pyy(i,1:(size/2))/b);
    end
    %----- plot spectrum
    cla
    hold on
    for i=1:1:chan
        plot(f,P(i,:));
    end
    hold off
    grid on;
    pause(0.1);
end
close all

function [z,Fs,chan,size] = sin_int16()
    chan = 4;           %channels
    Fs = 100000000;     %sampling frequence
    F = zeros(chan);    %channels frequence
    for j=1:1:chan
        F(j) = j*10000000;
    end
    size = 1024;
    z = int16(zeros(chan,size));
  
    for i=1:1:chan
        div = Fs/F(i);
        t = 0:1/div:(size-1)/div;
        r = randi([-16,16],[1,size],'int16');
        ph = randn;
        x = int16((32768/i)*sin(2*pi*t + ph));
        z(i,:) = x+r;
    end
   
end

end



