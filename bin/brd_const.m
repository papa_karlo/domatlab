%BRD Constants
BRDopen_EXCLUSIVE = hex2dec('0');	% Board must be exclusive
BRDopen_SHARED = hex2dec('1');		% Board may be shared
BRDopen_SPY = hex2dec('2');		% Board is used for monitoring

BRDdm_UNVISIBLE = hex2dec('0');	%Display nothing
BRDdm_VISIBLE   = hex2dec('F');	%Display all message
BRDdm_CONSOLE   = hex2dec('80');	%Display message to console
BRDdm_INFO      = hex2dec('1');	%Display info message
BRDdm_WARN      = hex2dec('2');	%Display warning message
BRDdm_ERROR     = hex2dec('4');	%Display error message
BRDdm_FATAL     = hex2dec('8');	%Display fatal error message

BRDcapt_EXCLUSIVE = hex2dec('0');	% Board must be exclusive

BRDstrm_DIR_IN = hex2dec('1');	% To HOST
BRDstrm_DIR_OUT = hex2dec('2');	% From HOST

MAX_SRV = uint32('16');	% max services

ADC_CREATE_FILE = hex2dec('1');     %!< ������ ������� ����
ADC_OPEN_FILE   = hex2dec('2');     %!< ��������� ����  (���� �� ����������, �� ���������)
ADC_FILE_RDONLY = hex2dec('10');    %!< ��������� ���� � ������ ������ ��� ������ (���� �� ����������, �� ������)
ADC_FILE_WRONLY = hex2dec('20');    %!< ��������� ���� � ������ ������ ��� ������ (���� �� ����������, �� ���������)
ADC_FILE_RDWR   = hex2dec('40');    %!< ��������� ���� � ������ ������/������ (���� �� ����������, �� ���������)
ADC_FILE_DIRECT = hex2dec('80');    %!< ��������� ���� � ������ ������� ������/������ (Linux)
