clear all
clear mex

brd_const;

disp(sprintf('Hello, EXAMADC+MATLAB!'));

brd('displayMode', BRDdm_VISIBLE);
brdCnt = brd('init','brd.ini');

brd('getoptions', 'exam_adc.ini');

lidItem = uint32(64); 
[lidList,lidItems] = brd('lidList',lidItem);

%BRD_Info Demo
%brdLid = uint32(4); 
brdLid = lidList(1); 
[brdName] = brd('getInfo', 'name', brdLid);
disp(sprintf('|BRD_Info|: BRD Name - "%s"', brdName)); 
[brdType] = brd('getInfo', 'type', brdLid);
disp(sprintf('|BRD_Info|: BRD Type - %08X', brdType)); 
[brdPid] = brd('getInfo', 'pid', brdLid);
disp(sprintf('|BRD_Info|: BRD PID  - %08X', brdPid)); 
[brdOrder] = brd('getInfo', 'order', brdLid);
disp(sprintf('|BRD_Info|: BRD Order- %d', brdOrder)); 
[brdBus] = brd('getInfo', 'bus', brdLid);
disp(sprintf('|BRD_Info|: BRD Bus  - %d', brdBus)); 
[brdDev] = brd('getInfo', 'dev', brdLid);
disp(sprintf('|BRD_Info|: BRD Dev  - %d', brdDev)); 
[brdBase] = brd('getInfo', 'base', brdLid);
for i=1:1:length(brdBase)
    fprintf('|BRD_Info|: BRD base- %08Xh\n', brdBase(i)); 
end

brdMode = int32(BRDopen_SHARED);
brdId = brd('open', brdLid, brdMode);

%Global Variables
nodeId = uint32(0);
sigId = uint32(0);
sigCnt = uint32(0);
timeout= uint32(5000);

%BRD_serviceList() Demo
% get items
srv_name = char(zeros(16,32));
srv_attr = uint32(zeros(16,1));
item = uint32(16);
items = uint32(0);
[items] = brd('serviceList', 'items', brdId, nodeId, item);
fprintf('|BRD_serviceList|: items = %d\n', items) 

% get services
for i=0:1:(items-1)
    [srv_name] = brd('serviceList', 'service', brdId, nodeId, i);    
    fprintf('|BRD_Info|: Service Name - %s\n', srv_name); 
end

hADC = uint32(0);
[hADC] = brd('capture', srv_name);
numChan = uint32(0);
[numChan] = brd('adcsettings', hADC, srv_name, 'exam_adc.ini');

retCode = uint32(0);
[retCode] = brd('reset', brdId, nodeId );
[retCode] = brd('close', brdId);
[retCode] = brd('cleanup');
