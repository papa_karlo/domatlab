clear all
clear mex

brd_const;

disp(sprintf('Hello, BARDY+MATLAB!'));

%BRD_displayMode Demo
brd('displayMode', BRDdm_VISIBLE);
brdCnt = brd('init','brd.ini');

%BRD_lidList Demo
lidItem = uint32(64); 
[lidList,lidItems] = brd('lidList',lidItem);

%BRD_Info Demo
brdLid = uint32(1); 
[brdName] = brd('getInfo', 'name', brdLid);
disp(sprintf('|BRD_Info|: BRD Name - "%s"', brdName)); 
[brdType] = brd('getInfo', 'type', brdLid);
disp(sprintf('|BRD_Info|: BRD Type - %08X', brdType)); 
[brdPid] = brd('getInfo', 'pid', brdLid);
disp(sprintf('|BRD_Info|: BRD PID  - %08X', brdPid)); 
[brdOrder] = brd('getInfo', 'order', brdLid);
disp(sprintf('|BRD_Info|: BRD Order- %d', brdOrder)); 
[brdBus] = brd('getInfo', 'bus', brdLid);
disp(sprintf('|BRD_Info|: BRD Bus  - %d', brdBus)); 
[brdDev] = brd('getInfo', 'dev', brdLid);
disp(sprintf('|BRD_Info|: BRD Dev  - %d', brdDev)); 
[brdBase] = brd('getInfo', 'base', brdLid);
for i=1:1:length(brdBase)
    disp(sprintf('|BRD_Info|: BRD base- %08Xh', brdBase(i))); 
end

brdMode = int32(BRDopen_SHARED);
brdId = brd('open', brdLid, brdMode);

%Global Variables
nodeId = uint32(0);
sigId = uint32(0);
%sigCnt = uint32(0);
timeout= uint32(5000);

%BRD_signalSend()&BRD_signalGrab() Demo
appName = 'brd_signal.bin';
brd('reset', brdId, int32(-1));
brd('load', brdId, nodeId, appName);
brd('start', brdId, 0 ); disp(sprintf('BRD Start - %s!',appName));
pause(0.1);
[sigCnt] = brd('signalGrab',brdId,nodeId,sigId,timeout); 
for j=1:1:16
    brd('signalSend',brdId,nodeId,sigId);
    [sigCnt] = brd('signalGrab',brdId,nodeId,sigId,timeout); 
    disp(sprintf('Cycle %d: Signal Counter - %d', j, sigCnt)); 
end

%BRD_writeFIFO()&BRD_readFIFO() Demo
appName = 'rwfifo.bin';
brd('reset', brdId, int32(-1));
brd('load', brdId, nodeId, appName);
brd('start', brdId, nodeId); disp(sprintf('BRD Start - %s!',appName)); 
%pause(1);
[sigCnt] = brd('signalGrab',brdId,nodeId,sigId,timeout); 
fw_array = uint32(zeros(1,4096));
fr_array = uint32(zeros(1,4096));
for i=1:1:4096 fw_array(i) = 16384+i; end;
for j=1:1:16
    brd('writeFIFO',brdId,nodeId,fw_array, uint32(4096*4),timeout);
    brd('readFIFO',brdId,nodeId,fr_array, uint32(4096*4),timeout);
    disp(sprintf('|BRD_RWFIFO|: RW FIFO Cycle - %d',fr_array(1))); 
end


%BRD_poke()&BRD_peek() Demo
brd_addr = uint32(hex2dec('C0000000'));
brd('poke',brdId,nodeId,brd_addr,uint32(hex2dec('12345678')));
val = brd('peek',brdId,nodeId,brd_addr); disp(sprintf('Val = %Xh',val)); 

%BRD_writeRAM()&BRD_readRAM() Demo
src_array = uint32(zeros(1,8));
for i=1:1:8 src_array(i) = i; end;
brd('writeRAM',brdId,nodeId,brd_addr,src_array,int32(8),int32(4));
dst_array = uint32(zeros(1,8));
brd('readRAM',brdId,nodeId,brd_addr,dst_array,int32(8),int32(4));
disp(sprintf('|BRD_RWRAM|: W=%d - R=%d',src_array(3),dst_array(3))); 

%BRD_writeDPRAM()&BRD_readDPRAM() Demo
for i=1:1:8 src_array(i) = i+1; end;
brd('writeDPRAM',brdId,nodeId, uint32(0), src_array, uint32(32));
brd('readDPRAM',brdId,nodeId, uint32(0), dst_array, uint32(32));
disp(sprintf('|BRD_RWDPRAM|: W=%d - R=%d',src_array(2),dst_array(2))); 

%BRD_write()&BRD_read() Demo
appName = 'brd_r_w.bin';
brd('reset', brdId, int32(-1));
ret = brd('load', brdId, 0, appName);
brd('start', brdId, 0 ); disp(sprintf('BRD Start - %s!', appName)); 
pause(0.1);
[sigCnt] = brd('signalGrab',brdId,nodeId,sigId,timeout); 
w_array = uint32(zeros(1,128));
r_array = uint32(zeros(1,128));
for i=1:1:128 w_array(i) = i; end;
brd('write',brdId,nodeId,w_array, uint32(128*4),timeout);
%pause(0.1);
brd('read',brdId,nodeId,r_array, uint32(128*4),timeout);
disp(sprintf('|BRD_RW|: W=%d - R=%d',w_array(10),r_array(10))); 

%BRD_putMsg()&BRD_getMsg() Demo
appName = 'brd_msg.bin';
brd('reset', brdId, int32(-1));
argc = uint32(2);
ret = brd('load', brdId, 0, appName, argc, '1234', '5678');
brd('start', brdId, 0 ); disp(sprintf('BRD Start - %s!',appName)); 
[sigCnt] = brd('signalGrab',brdId,nodeId,sigId,timeout); 
pause(0.1);
mw_array = uint32(zeros(1,128));
mr_array = uint32(zeros(1,128));
for i=1:1:128 
    mw_array(i) = i+128; 
    mr_array(i) = i+256; 
end;
brd('putMsg',brdId,nodeId,mw_array, uint32(128*4),-1);
%pause(0.1);
brd('getMsg',brdId,nodeId,mr_array,uint32(128*4),-1);
disp(sprintf('|BRD_MSG|: W=%d - R=%d',mw_array(10),mr_array(10))); 

appName = 'blink.bin';
brd('reset', brdId, int32(-1));
ret = brd('load', brdId, nodeId, appName);
brd('start', brdId, nodeId ); disp(sprintf('BRD Start - %s!',appName)); 

brd('reset', brdId, nodeId );
brd('close', brdId);
brd('cleanup');
clear mex;
