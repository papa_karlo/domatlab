
clear all
clear mex

brd_const

fprintf('Hello, DEV&ADC + MATLAB!\n');

brd('displayMode', BRDdm_VISIBLE);

%Open device
idev = int32(0);
[hdev,numdev] = brd('DEV_open', 'brd.ini', idev);
if hdev <= 0 
    fprintf('#ERROR by BARDY Initialization - %d\n', hdev);    
    brd('cleanup');
    exit;
end
fprintf('Number of Devices = %d\n', numdev);    

%Get Device Info 
[devName] = brd('DEV_info', hdev, idev, 'devName');
[pldName] = brd('DEV_info', hdev, idev, 'pldName');
[subName] = brd('DEV_info', hdev, idev, 'subName');

[devID] = brd('DEV_info', hdev, idev, 'devID');
[rev] = brd('DEV_info', hdev, idev, 'rev');
[pid] = brd('DEV_info', hdev, idev, 'pid');
[bus] = brd('DEV_info', hdev, idev, 'bus');
[dev] = brd('DEV_info', hdev, idev, 'dev');
[slot] = brd('DEV_info', hdev, idev, 'slot');
[pwrOn] = brd('DEV_info', hdev, idev, 'pwrOn');
[pwrValue] = brd('DEV_info', hdev, idev, 'pwrValue');
[pldVer] = brd('DEV_info', hdev, idev, 'pldVer');
[pldMod] = brd('DEV_info', hdev, idev, 'pldMod');
[pldBuild] = brd('DEV_info', hdev, idev, 'pldBuild');
[subType] = brd('DEV_info', hdev, idev, 'subType');
[subVer] = brd('DEV_info', hdev, idev, 'subVer');
[subPID] = brd('DEV_info', hdev, idev, 'subPID');

if (devID==hex2dec('53B1')) || (devID==hex2dec('53B3'))
    fprintf('%s %x.%x (0x%x): Bus = %d, Dev = %d, G.adr = %d, Order = %d, PID = %d\n', ...
        devName, bitshift(rev,-4), bitand(rev,hex2dec('f')), devID, bus, dev, ...
        bitand(slot,hex2dec('ffff')), bitshift(pid,-28), bitand(pid,hex2dec('ffffffff'))); 
else
    fprintf('%s %x.%x (0x%x): Bus = %d, Dev = %d, slot = %d, PID = %d\n', ...
        devName, bitshift(rev,-4), bitand(rev,hex2dec('f')), devID, bus, dev, slot, pid); 
end

if pldVer~=hex2dec('ffff')
    fprintf('%s: Version %d.%d, Modification %d, Build 0x%X\n', ...
        pldName, bitshift(pldVer,-8), bitand(pldVer,hex2dec('ff')), pldMod, pldBuild); 
end

if (pwrOn~=0) && (pwrOn~=hex2dec('ff'))
    fprintf('FMC Power: ON %.2f Volt\n', pwrValue/100.0);
else 
    fprintf('FMC Power: OFF %.2f Volt\n', pwrValue/100.0);
end

if (subType~=hex2dec('ffff'))
    fprintf('Subm: %s %x.%x (%X), Serial Number = %d\n', ...
        subName, bitshift(subVer,-4), bitand(subVer,hex2dec('f')), subType, subPID); 
end

[items] = brd('DEV_info', hdev, idev, 'srvItems');
for i=0:1:double(items-1)
    idx = int32(i);
    [srvName] = brd('DEV_info', hdev, idev, 'srvName', idx);
    fprintf('Service %d: %s\n', idx, srvName); 
end

%AdcSrvName = 'FM412x500M0';
AdcSrvName = 'ADCFM216x250M0';
[hadc] = brd('ADC_open', hdev, AdcSrvName); 

[bits] = brd('ADC_cfg', hadc, 'Bits'); 
[encoding] = brd('ADC_cfg', hadc, 'Encoding'); 
[minrate] = brd('ADC_cfg', hadc, 'MinRate'); 
[maxrate] = brd('ADC_cfg', hadc, 'MaxRate'); 
[inprange] = brd('ADC_cfg', hadc, 'InpRange'); 
[fifosize] = brd('ADC_cfg', hadc, 'FifoSize'); 
[numchans] = brd('ADC_cfg', hadc, 'NumChans'); 
[chantype] = brd('ADC_cfg', hadc, 'ChanType'); 

clkValue = double(0.);
rate = double(0.);
use_mem = uint32(0);
[ret,chmask,clksrc,clkvalue,rate] = brd('ADC_set', hadc, idev, AdcSrvName, 'exam_adc.ini', use_mem); 
if ret<0 
    fprintf('#ERROR by ADC_set - %d\n', ret);    
    brd('ADC_close', hadc);
    brd('DEV_close', hdev); 
    brd('cleanup');
    exit;
end

fprintf('ADC Config: %d Bits, FIFO is %d kBytes, %d channels\n', ...
    bits, fifosize/1024, numchans);
fprintf('           Min rate = %d kHz, Max rate = %d MHz\n', ...
    minrate/1000, maxrate/1000000);
fprintf('           Input range = %.3f V\n', double(inprange)/1000.);

fprintf('ADC channel mask = %0X\n', chmask);
fprintf('ADC clocking: source = %d, value = %.2f MHz, rate = %.3f kHz\n', ...
    clksrc, clkvalue/1000000., rate/1000.);

blksize = uint32(hex2dec('10000'));
blknum = uint32(2);
dir = uint32(BRDstrm_DIR_IN);
iscont = uint32(0);
[ret] = brd('ADC_allocbuf', hadc, blksize, blknum, dir, iscont); 
if ret==-1
    fprintf('ADC_allocbuf is error - %d\n', ret);
    brd('ADC_close', hadc);
    brd('DEV_close', hdev); 
    brd('cleanup');
    exit;
end
[ret] = brd('ADC_prepare', hadc); 

fprintf('ADC is starting ...\n');

%%for ii=1:1:10000
while true
%{    
    key = get(groot,'factory');
        if (strcmp(key , 'return'))
    end
  %}  
    [ret, databuf] = brd('ADC_read', hadc, use_mem); 
%%{
    fs_flag = uint32(ADC_CREATE_FILE+ADC_FILE_WRONLY);
    [fs] = brd('ADC_openFile', 'my_file.flg', fs_flag);
    flg_data = uint32(zeros(1,2));
    flg_data(1) = hex2dec('0');
    flg_data(2) = hex2dec('0');
    s = whos('flg_data'); 
    file_size = uint32(s.bytes);
    [ret] = brd('ADC_writeFile', fs, flg_data, file_size);
    [ret] = brd('ADC_closeFile', fs);
%%}
    %----- write to file
    %fid = fopen('my_file.bin', 'wb');     % �������� ����� �� ������ 
    [fid] = brd('ADC_openFile', 'my_file.bin', fs_flag);
    if fid == -1                     % �������� ������������ �������� 
        error('Data File is not opened'); 
    end 
    s = whos('databuf'); 
    file_size = uint32(s.bytes);
    [ret] = brd('ADC_writeFile', fid, databuf, file_size);
    %fwrite(fid, databuf, 'int16');   % ������ ������� � ���� 
    [ret] = brd('ADC_closeFile', fid);
    %fclose(fid);                % �������� ����� 
    %-------------------

    [fs] = brd('ADC_openFile', 'my_file.flg', fs_flag);
    flg_data = uint32(zeros(1,2));
    flg_data(1) = hex2dec('FFFFFFFF');
    flg_data(2) = hex2dec('0');
    s = whos('flg_data'); 
    file_size = uint32(s.bytes);
    [ret] = brd('ADC_writeFile', fs, flg_data, file_size);
    [ret] = brd('ADC_closeFile', fs);

end

brd('ADC_freebuf', hadc, blknum, iscont);

[ret] = brd('ADC_close', hadc);
[ret] = brd('DEV_close', hdev);

close all




