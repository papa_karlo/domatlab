hf = figure('Name','RANDI_INT16_INT8');
for i=1:1:100
%in1 = randi([-1023,1023],[1,512],'int16');
in1 = randi_mex([-1023,1023],[1,512],'int16');
%in2 = randi([-127,127],[1,512],'int8');
in2 = randi_mex([-127,127],[1,512],'int8');
subplot(2,1,1); 
plot(in1,'r'); grid on; title('Random INT16');
xlabel('index'); ylabel('value');
subplot(2,1,2); 
plot(in2); grid on; title('Random INT8');
xlabel('index'); ylabel('value');
pause(0.1);
end
close('RANDI_INT16_INT8');