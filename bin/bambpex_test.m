
clear all
clear mex

brd_const

fprintf('Hello, DAQ+MATLAB!\n');

brd('displayMode', BRDdm_VISIBLE);
[ret] = brd('init', 'brd.ini');
if ret <= 0 
    fprintf('init - %d\n', ret);    
    brd('cleanup');
    exit;
end
fprintf('init - %d\n', ret);

[ret] = brd('getoptions', 'exam_adc.ini');
fprintf('getoptions - %d\n', ret);

lidItem = uint32(64); 
[lidList,lidItems] = brd('lidList',lidItem);

%BRD_Info Demo
brdLid = uint32(1); 
[brdName] = brd('getInfo', 'name', brdLid);
fprintf('|BRD_Info|: BRD Name - "%s"\n', brdName); 
[brdType] = brd('getInfo', 'type', brdLid);
fprintf('|BRD_Info|: BRD Type - %08X\n', brdType); 
[brdPid] = brd('getInfo', 'pid', brdLid);
fprintf('|BRD_Info|: BRD PID  - %08X\n', brdPid); 
[brdOrder] = brd('getInfo', 'order', brdLid);
fprintf('|BRD_Info|: BRD Order- %d\n', brdOrder); 
[brdBus] = brd('getInfo', 'bus', brdLid);
fprintf('|BRD_Info|: BRD Bus  - %d\n', brdBus); 
[brdDev] = brd('getInfo', 'dev', brdLid);
fprintf('|BRD_Info|: BRD Dev  - %d\n', brdDev); 
[brdBase] = brd('getInfo', 'base', brdLid);
for i=1:1:length(brdBase)
    fprintf('|BRD_Info|: BRD base- %08Xh\n', brdBase(i)); 
end

brdMode = int32(BRDopen_SHARED);
brdId = brd('open', brdLid, brdMode);

%Global Variables
nodeId = uint32(0);
sigId = uint32(0);
sigCnt = uint32(0);
timeout= uint32(5000);

%BRD_serviceList() Demo
% get items
srv_name = char(zeros(16,32));
srv_attr = uint32(zeros(16,1));
item = uint32(16);
items = uint32(0);
[items] = brd('serviceList', 'items', brdId, nodeId, item);
fprintf('|BRD_serviceList|: items = %d\n', items); 

% get services
for i=0:1:(items-1)
    [srv_name] = brd('serviceList', 'service', brdId, nodeId, i);    
    fprintf('|BRD_Info|: Service Name - %s\n', srv_name); 
end

timeout = int32(10000);
mode = uint32(BRDcapt_EXCLUSIVE);
[servId,retmode] = brd('capture', brdId, nodeId, mode, 'FM412x500M0', timeout);

[ret,bits,encoding,minrate,maxrate,inprange,fifosize,numchans,chantype] = brd('ctrl', servId, 0, 'BRDctrl_ADC_GETCFG');

[ret] = brd('ctrl', servId, 0, 'BRDctrl_ADC_READINIFILE','exam_adc.ini', 'device0_FM412x500M0');

clkValue = double(0);
rate = double(0);
[ret,clkSrc,clkValue,rate] = brd('ctrl', servId, 0, 'BRDctrl_ADC_GETSYNCMODE');

[ret,chanMask] = brd('ctrl', servId, 0, 'BRDctrl_ADC_GETCHANMASK');

blkSize = uint32(hex2dec('10000'));
blkNum  = uint32(4);
dir = uint32(BRDstrm_DIR_IN);
isCont = uint32(0);
[ret] = brd('allocbuf', servId, blkSize, blkNum, dir, isCont);

[ret] = brd('freebuf', servId, blkNum, isCont);

[ret] = brd('reset', brdId, nodeId );
[ret] = brd('close', brdId);
[ret] = brd('cleanup');
close all




